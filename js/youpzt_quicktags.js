QTags.addButton( 'hr', 'hr', '<hr/>', '' ); //快捷输入一个hr横线，点一下即可
QTags.addButton( 'btn', 'btn的css类', 'class="uk-button uk-button-primary"', '' ); //快捷输入btn快捷按钮
QTags.addButton( 'h3', 'h3', '<h3>', '</h3>' ); //快捷输入h3标签
QTags.addButton( 'h4', 'h4', '<h4>', '</h4>' ); //快捷输入h3标签
//QTags.addButton( 'my_id', 'my button', 'n', 'n' );
//这儿共有四对引号，分别是按钮的ID、显示名、点一下输入内容、再点一下关闭内容（此为空则一次输入全部内容），n表示换行。

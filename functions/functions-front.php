<?php
//移除菜单的多余CSS选择器
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
//add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);
function my_css_attributes_filter($var) {
	return is_array($var) ? array_intersect($var, array('current-menu-item','current-post-ancestor','current-menu-ancestor','current-menu-parent')) : '';
}
//取消描述中的p标签
function deletehtml($description) {  
    $description = trim($description);  
    $description = strip_tags($description,"");  
    return ($description); 
} 
add_filter('category_description', 'deletehtml');
	// category id in body and post class
	function category_id_class($classes) {
		global $post;
		foreach((get_the_category($post->ID)) as $category)
			$classes [] = 'cat-' . $category->cat_ID . '-id';
			return $classes;
	}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');

/**
 * WordPress 添加面包屑导航 
 * 
 */
function cmp_breadcrumbs(){
	$delimiter = '»'; // 分隔符
	$before = '<span class="current">'; // 在当前链接前插入
	$after = '</span>'; // 在当前链接后插入
	if ( !is_home() && !is_front_page() || is_paged() ) {	
		global $post;
			# code...
			echo '<span itemscope itemtype="http://schema.org/WebPage" id="crumbs">';
			$homeLink = home_url();
			echo ' <a itemprop="breadcrumb" class="color-6" href="' . $homeLink . '">' . __( '首页' , 'cmp' ) . '</a> ' . $delimiter . ' ';
		
		if ( is_category() ) { // 分类 存档
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0){
				$cat_code = get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ');
				echo $cat_code = str_replace ('<a','<a itemprop="breadcrumb"', $cat_code );
			}
			echo $before . '' . single_cat_title('', false) . '' . $after;
		} elseif ( is_day() ) { // 天 存档
			echo '<a itemprop="breadcrumb" href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo '<a itemprop="breadcrumb"  href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time('d') . $after;
		} elseif ( is_month() ) { // 月 存档
			echo '<a itemprop="breadcrumb" href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time('F') . $after;
		} elseif ( is_year() ) { // 年 存档
			echo $before . get_the_time('Y') . $after;
		} elseif ( is_single() && !is_attachment() ) { // 文章
			if ( get_post_type() != 'post' ) { // 自定义文章类型
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<a itemprop="breadcrumb" href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
				echo $before . get_the_title() . $after;
			} else { // 文章 post
				$cat = get_the_category(); $cat = $cat[0];
				$cat_code = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				echo $cat_code = str_replace ('<a','<a itemprop="breadcrumb"', $cat_code );
				echo $before . '正文' . $after;
			}
		}elseif ( is_attachment() ) { // 附件
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo '<a itemprop="breadcrumb" href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
			echo $before . get_the_title() . $after;
		} elseif ( is_page() && !$post->post_parent ) { // 页面
			echo $before . get_the_title() . $after;
		} elseif ( is_page() && $post->post_parent ) { // 父级页面
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a itemprop="breadcrumb" href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
			echo $before . get_the_title() . $after;
		} elseif ( is_search() ) { // 搜索结果
			echo $before ;
			printf( __( '%s 的搜索结果', 'cmp' ),  get_search_query() );
			echo  $after;
		} elseif ( is_tag() ) { //标签 存档
			echo $before ;
			printf( __( '标签 %s 的文章归档', 'cmp' ), single_tag_title( '', false ) );
			echo  $after;
		} elseif ( is_author() ) { // 作者存档
			global $author;
			$userdata = get_userdata($author);
			echo $before ;
			printf( __( '作者 %s 的文章归档', 'cmp' ),  $userdata->display_name );
			echo  $after;
		} elseif ( is_404() ) { // 404 页面
			echo $before;
			echo 'Not Found';
			echo  $after;
		}elseif ( !is_single() && !is_page() && get_post_type() != 'post' ) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;
		}else{
			echo '未知';
		}
		if ( get_query_var('paged') ) { // 分页
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() )
				echo sprintf( __( '(第%s页)', 'cmp' ), get_query_var('paged') );
		}
		echo '</span>';
	}
}

//图片添加alt属性
function image_alt( $imgalt ){
    global $post;
    $title = $post->post_title;
    $imgUrl = "<img\s[^>]*src=(\"??)([^\" >]*?)\\1[^>]*>";
    if(preg_match_all("/$imgUrl/siU",$imgalt,$matches,PREG_SET_ORDER)){
        if( !empty($matches) ){
                for ($i=0; $i < count($matches); $i++){
                    $tag = $url = $matches[$i][0];
                    $judge = '/alt=/';
                    preg_match($judge,$tag,$match,PREG_OFFSET_CAPTURE);
                    if( count($match) < 1 )
                    $altURL = ' alt="'.$title.'" ';
                    $url = rtrim($url,'>');
                    $url .= $altURL.'>';
                    $imgalt = str_replace($tag,$url,$imgalt);
                }
        }
    }
    return $imgalt;
}

add_filter( 'the_content','image_alt');

//禁止代码标点转换
remove_filter('the_content', 'wptexturize');
//分页
if ( ! function_exists( 'youpzt_paging' ) ) :
function youpzt_paging() {
    $p = 4;
    if ( is_singular() ) return;
    global $wp_query, $paged;
    $max_page = $wp_query->max_num_pages;
    if ( $max_page == 1 ) return; 
    echo '<div class="pagination">';
    if ( empty( $paged ) ) $paged = 1;
    // echo '<span class="pages">Page: ' . $paged . ' of ' . $max_page . ' </span> '; 
    previous_posts_link('上一页'); 

    if ( $paged > $p + 1 ) p_link( 1, '<a href="#">第一页</a>' );
    if ( $paged > $p + 2 ) echo "<span>···</span>";
    for( $i = $paged - $p; $i <= $paged + $p; $i++ ) { 
        if ( $i > 0 && $i <= $max_page ) $i == $paged ? print '<a class="current" href="'.esc_html( get_pagenum_link( $i ) ).'">'.$i.'</a>' : p_link( $i );
    }
    if ( $paged < $max_page - $p - 1 ) echo "<span> ... </span>";
    //if ( $paged < $max_page - $p ) p_link( $max_page, '&raquo;' );
    next_posts_link('下一页'); 
    // echo '<li><span>共 '.$max_page.' 页</span></li>';
    echo '</div>';
}
function p_link( $i, $title = '' ) {
    if ( $title == '' ) $title = "第 {$i} 页";
    echo "<a href='", esc_html( get_pagenum_link( $i ) ), "'>{$i}</a>";
}
endif;

function post_views($before=' ', $after=' ', $echo = 1)
{
	  global $post;
	  $post_ID = $post->ID;
	  $views = (int)get_post_meta($post_ID, 'views', true);
	  if ($echo) echo $before, number_format($views), $after;
	  else return $views;
};
/**
 * Enqueue scripts and styles.
 */
 if ( ! function_exists( 'youpzt_of_register_assets' ) ) :
function youpzt_of_register_assets() {	
	wp_enqueue_style( 'base-css', get_template_directory_uri() . '/css/base.css');
	wp_enqueue_style( 'youpzt-style', get_stylesheet_uri(),'',1.14);//加载主题目录的style.css
	if(is_home()){

	}
	if (wp_is_mobile()) {//手机端加载

	}

	/*if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}*/
}
endif; 
add_action( 'wp_enqueue_scripts', 'youpzt_of_register_assets' );

/* 取消原有jQuery*/
function headerScript() {
    if ( !is_admin() ) {
        wp_deregister_script( 'jquery' );
        //wp_enqueue_script('jquery','http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js',array(),'2.1.1');
        wp_enqueue_script('jquery','http://libs.baidu.com/jquery/1.11.3/jquery.min.js',array(),'2.0.1');
        //wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.10.1.min.js', array(), '1.10');
        //wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '1.11');
        wp_enqueue_script( 'jquery' );   
    }  
}  
add_action( 'init', 'headerScript' );

/* 
 * timeago
 * ====================================================
*/
function time_ago( $type = 'commennt', $day = 7 ) {
  $d = $type == 'post' ? 'get_post_time' : 'get_comment_time';
  if (time() - $d('U') > 60*60*24*$day) return;
  echo ' (', human_time_diff($d('U'), strtotime(current_time('mysql', 0))), '前)';
}
function timeago( $ptime ) {
    $ptime = strtotime($ptime);
    $etime = time() - $ptime;
    if($etime < 1) return '刚刚';
    $interval = array (
        12 * 30 * 24 * 60 * 60  =>  '年前 ('.date('Y-m-d', $ptime).')',
        30 * 24 * 60 * 60       =>  '个月前 ('.date('m-d', $ptime).')',
        7 * 24 * 60 * 60        =>  '周前 ('.date('m-d', $ptime).')',
        24 * 60 * 60            =>  '天前',
        60 * 60                 =>  '小时前',
        60                      =>  '分钟前',
        1                       =>  '秒前'
    );
    foreach ($interval as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . $str;
        }
    };
}

//支持外链缩略图
 function catch_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];
if(empty($first_img)){
		$random = mt_rand(1, 20);
		$first_img='http://websucai.qiniudn.com/webtabtb'.$random.'.jpg';	
		/*$first_img=get_bloginfo ( 'stylesheet_directory' );
		$first_img='/images/random/tb'.$random.'.jpg';*/
  }
  return $first_img;
 }

//缩略图裁剪
function youpzt_thumbnail( $width = 300,$height =280,$is_caijian=true ){
    global $post;
		$width_height=' width="'.$width.'" height="'.$height.'" ';
		$attachment_w_h_class='class="attachment-'.$width.'x'.$height.' wp-post-image" ';
	if($is_caijian==true){//是否裁剪
		$timthumb_url=get_bloginfo("template_url").'/inc/timthumb.php?src=';//压缩图片
		$w_h='&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1';//附加
	}else{
		$timthumb_url='';
		$w_h='';
	}
    if( has_post_thumbnail() ){    //如果有特色图像，则显示特色图像
        $timthumb_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
        $post_timthumb = '<img src="'.$timthumb_url.$timthumb_src[0].$w_h.'" alt="'.$post->post_title.'" '.$width_height.$attachment_w_h_class.' />';
        return $post_timthumb;
    } else {
        $post_timthumb = '';
        ob_start();
        ob_end_clean();
        $output = preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $index_matches);    //获取日志中第一张图片
        $first_img_src = $index_matches [1];    //获取该图片 src
        if( !empty($first_img_src) ){    //如果日志中有图片
            $post_timthumb = '<img src="'.$timthumb_url.$first_img_src.$w_h.'" alt="'.$post->post_title.'" '.$width_height.$attachment_w_h_class.' />';
        } else {    //如果日志中没有图片，则显示默认
			$random = mt_rand(1, 4);
            	$post_timthumb = '<img src="'.$timthumb_url.get_bloginfo("template_url").'/images/thumb/tb-'.$random.'.jpg'.$w_h.'" alt="'.$post->post_title.'"  '.$width_height.$attachment_w_h_class.' />';
        }
        return $post_timthumb;
    }
}
//缩略图裁剪src
function youpzt_thumbnail_src( $width = 300,$height =280 ,$is_caijian=true){
    global $post;
		if($is_caijian==true){//是否裁剪
		$timthumb_url=get_bloginfo("template_url").'/inc/timthumb.php?src=';//压缩图片
		$w_h='&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1';//附加
	}else{
		$timthumb_url='';
		$w_h='';
	}
    if( has_post_thumbnail() ){    //如果有缩略图，则显示缩略图
        $timthumb_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
        $post_timthumb_src = get_bloginfo("template_url").'/inc/timthumb.php?src='.$timthumb_src[0].'&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1';
        return $post_timthumb_src;
    } else {
        $post_timthumb_src = '';
        ob_start();
        ob_end_clean();
        $output = preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $index_matches);    //获取日志中第一张图片
        $first_img_src = $index_matches [1];    //获取该图片 src
        if( !empty($first_img_src) ){    //如果日志中有图片
            $post_timthumb_src = $timthumb_url.$first_img_src.$w_h;
        } else {    //如果日志中没有图片，则显示默认
			$random = mt_rand(1, 4);
            	$post_timthumb_src =get_bloginfo("template_url").'/inc/timthumb.php?src='.get_bloginfo("template_url").'/images/thumb/tb-'.$random.'.jpg&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1';
        }
        return $post_timthumb_src;
    }
}
?>
<?php 
//add_filter( 'pre_option_link_manager_enabled', '__return_true');
remove_action ('pre_post_update', 'wp_save_post_revision' );/*移除修订版本记录功能*/
add_action('wp_print_scripts','deel_disable_autosave' );
//移除自动保存
function deel_disable_autosave() {
  wp_deregister_script('autosave');
}
//文章版本
add_action('admin_init', create_function( '$a', "remove_action('post_updated','wp_save_post_revision' );"));//新版

create_function( '$a', "wp_deregister_script('autosave');");
//移除插件自动更新
add_action( 'load-plugins.php', create_function('$a', "remove_action('load-plugins.php', 'wp_update_plugins');"), 3);
add_filter('pre_site_transient_update_plugins', create_function('$a', "return null;")); // 关闭插件提示
add_action( 'load-update.php', create_function('$a', "remove_action('load-update.php', 'wp_update_plugins');"), 3);
add_action( 'load-update-core.php', create_function('$a', "remove_action('load-update-core.php', 'wp_update_plugins');"), 3);
add_action( 'admin_init', create_function('$a', "remove_action('admin_init', '_maybe_update_plugins');"), 3);
add_action( 'wp_update_plugins', create_function('$a', "remove_action('wp_update_plugins', 'wp_update_plugins');"), 3);
//关闭主题更新
add_action( 'load-themes.php', create_function('$a', "remove_action('load-themes.php', 'wp_update_themes');"), 3);
add_filter('pre_site_transient_update_themes',  create_function('$a', "return null;")); // 关闭主题提示
add_action( 'load-update.php', create_function('$a', "remove_action('load-update.php', 'wp_update_themes');"), 3);
add_action( 'load-update-core.php', create_function('$a', "remove_action('load-update-core.php', 'wp_update_themes');"), 3);
add_action( 'admin_init', create_function('$a', "remove_action('admin_init', '_maybe_update_themes');"), 3);
add_action( 'wp_update_themes', create_function('$a', "remove_action('wp_update_themes', 'wp_update_themes');"), 3);

add_action( 'admin_init', create_function('$a', "remove_action('admin_init', '_maybe_update_core');"), 3);// 禁止 WordPress 检查更新  
add_action( 'wp_version_check', create_function('$a', "remove_action('wp_version_check', 'wp_version_check');"), 3);
//add_action( 'plugins_loaded', create_function( '$a', "remove_action('init', 'wp_version_check');"), 20);
add_action( 'init', create_function( '$a', "remove_action('init', 'wp_version_check_mod');"), 3); // For WPCNG

remove_action( 'load-update-core.php', 'wp_update_core' );// 移除核心更新的加载项
add_filter('pre_site_transient_update_core',create_function('$a', "return null;")); // 关闭核心提示
//add_action('init', 'wp_version_check_mod');
//remove_action('init', 'wp_version_check_mod', 13);
//移除文章类型meta模块
function youpzt_remove_meta(){
    if( !current_user_can('administrator') ) {
            /*post*/
              //remove_meta_box('postexcerpt', 'post', 'normal');
              remove_meta_box('trackbacksdiv', 'post', 'normal');
              remove_meta_box('postcustom', 'post', 'normal');
              remove_meta_box('revisionsdiv', 'post', 'normal');
              remove_meta_box('authordiv', 'post', 'normal');
              remove_meta_box('sqpt-meta-tags', 'post', 'normal');
    }
}
add_action( 'admin_menu', 'youpzt_remove_meta' );
//移除菜单
function remove_menus(){
global $menu,$current_user;
if ($current_user->user_level <10) {
    # code...
    $restricted=array(_('Dashboard'),_('Posts'),_('Profile'),_('Media'),_('Links'),_('Appearance'),__('Tools'),__('Users'),__('Settings'),__('Plugins'));
}else{
    $restricted=array();
}
end($menu);
        while(prev($menu)){
        $value=explode(' ',$menu[key($menu)][0]);
        if(strpos($value[0],'<')==FALSE){
                if(in_array($value[0]!=NULL ? $value[0]:"",$restricted)){
                unset($menu[key($menu)]);
                }
        }else{
                    $value2=explode('<',$value[0]);
                    if(in_array($value2[0]!=NULL ? $value2[0]:"",$restricted)){
                    unset($menu[key($menu)]);
                    }
            }
}
}
function remove_submenu(){
    remove_submenu_page('options-general.php','options-privacy.php');
}
add_action('admin_menu','remove_menus');
add_action('admin_init','remove_submenu');
//添加菜单
add_action('admin_menu', 'add_admin_menus');
function add_admin_menus(){
    //add_menu_page('案例', '案例展示管理', 'edit_private_posts', 'edit.php?category_name=cases', '','dashicons-feedback',6);//后台添加合集菜单
	//add_menu_page( '模板编辑','模板编辑', 'manage_options', 'theme-editor.php', '','dashicons-welcome-write-blog',59);
	add_menu_page( '菜单管理','菜单管理', 'manage_options', 'nav-menus.php', '','dashicons-menu',60);
}

add_action( 'admin_menu', 'change_post_menu_label' );

function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = '内容管理';
	 $submenu['edit.php'][5][0] = '所有内容';
    $submenu['edit.php'][10][0] = '添加内容';
    echo '';
}
//加载后台文章编辑自定义按钮
add_action('admin_print_scripts', 'youpzt_quicktags');
function youpzt_quicktags() {
    wp_enqueue_script(
        'youpzt_quicktags',
        get_stylesheet_directory_uri().'/js/youpzt_quicktags.js',
        array('quicktags')
    );
    }
//编辑器增强
 function enable_more_buttons($buttons) {
     $buttons[] = 'hr';
     $buttons[] = 'del';
     $buttons[] = 'sub';
     $buttons[] = 'sup'; 
     $buttons[] = 'fontselect';
     $buttons[] = 'fontsizeselect';
     $buttons[] = 'cleanup';   
     $buttons[] = 'styleselect';
     $buttons[] = 'wp_page';
     $buttons[] = 'anchor';
     $buttons[] = 'backcolor';
     return $buttons;
     }
add_filter("mce_buttons_3", "enable_more_buttons");
//移除
remove_action('welcome_panel', 'wp_welcome_panel');
function remove_screen_options(){ return false;}
//add_filter('screen_options_show_screen', 'remove_screen_options');

add_filter( 'contextual_help', 'wpse50723_remove_help', 999, 3 );
function wpse50723_remove_help($old_help, $screen_id, $screen){
        $screen->remove_help_tabs();
        return $old_help;
    }
//移除仪表盘
function disable_dashboard_widgets() {
    global $wp_meta_boxes,$current_user;
    // wp..
    if ($current_user->user_level <10) {
      unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
      //以下这一行代码将删除 "近期评论" 模块
      unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    }
    //以下这一行代码将删除 "快速发布" 模块
    //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
 
    //以下这一行代码将删除 "引入链接" 模块
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
 
    //以下这一行代码将删除 "插件" 模块
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
 
    //以下这一行代码将删除 "近期草稿" 模块
    //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
 
    //以下这一行代码将删除 "WordPress 开发日志" 模块
    //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
 
    //以下这一行代码将删除 "其它 WordPress 新闻" 模块
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
 
    //以下这一行代码将删除 "概况" 模块
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
}
add_action('wp_dashboard_setup', 'disable_dashboard_widgets', 999);

//每个作者只能浏览自己的文章
function youpzt_parse_query_useronly( $wp_query ) {
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/edit.php' ) !== false) {
        if ( !current_user_can( 'manage_options' ) ) {
            global $current_user;
            $wp_query->set( 'author', $current_user->id );
        }

    }

}
 
add_filter('parse_query', 'youpzt_parse_query_useronly' );
/**
 * WordPress 去除后台标题中的“—— WordPress”
 * 参考代码见 https://core.trac.wordpress.org/browser/tags/4.2.2/src/wp-admin/admin-header.php#L44
 */
add_filter('admin_title', 'youpzt_custom_admin_title', 10, 2);
function youpzt_custom_admin_title($admin_title, $title){
    return $title.' &lsaquo; '.get_bloginfo('name');
}
/**
 * 自定义 WordPress 后台底部的版权和版本信息
 */
add_filter('admin_footer_text', 'left_admin_footer_text'); 
function left_admin_footer_text($text) {
  // 左边信息
  $text = '<span id="footer-thankyou">感谢使用<a href="http://www.youpzt.com" target="_blank" rel="nofollow">优品定制</a>进行创作</span>'; 
  return $text;
}
add_filter('update_footer', 'right_admin_footer_text', 11); 
function right_admin_footer_text($text) {
  // 右边信息,默认显示版本号
  $text = "";
  return $text;
}

//后台只显示当前用户文章关联的评论
function wpdx_get_comment_list_by_user($clauses) {
    if (is_admin()){
          global $user_ID, $wpdb;
          $clauses['join'] = ", wp_posts";
          $clauses['where'] .= " AND wp_posts.post_author = ".$user_ID." AND wp_comments.comment_post_ID = wp_posts.ID";
    };
    return $clauses;
};
if(!current_user_can('edit_others_posts')) {
add_filter('comments_clauses', 'wpdx_get_comment_list_by_user');
}
?>
<?php
// 产品展示
function post_type_products() {
register_post_type(
		 'product', 
		 array( 'labels'=>array(
				'name' => _x('产品', 'post type general name'),
				'singular_name' => _x('产品', 'post type singular name'),
				'add_new' => _x('添加产品', 'addProducts'),
				'add_new_item' => __('添加产品'),
				'edit_item' => __('编辑产品'),
				'new_item' => __('新的产品'),
				'view_item' => __('预览产品'),
				'search_items' => __('搜索产品'),
				'not_found' =>  __('您还没有发布wordpress产品'),
				'not_found_in_trash' => __('回收站中没有产品'), 
				'parent_item_colon' => ''
				),
				'description'=>'发布优品主题原创作品',
				'public' => true,
				'publicly_queryable' => true,
				'hierarchical' => false,
				 'show_ui' => true,
				 'menu_icon'=>'dashicons-cart',
				 'menu_position'=>8,
				'supports' => array(
						'title',
						//'author', 
						'excerpt',
						'thumbnail',
						//'trackbacks',
						'editor', 
						'comments'
						//'custom-fields',
						//'revisions'	
						) ,
				//'taxonomies'=>array('category','post_tag'),//添加已经注册了的分类法(比如默认的分类、标签)
				) 
		  ); 

} 
add_action('init', 'post_type_products');

function create_product_taxonomy() 
{
  $labels = array(
		  'name' => _x( '产品分类', 'taxonomy general name' ),
		  'singular_name' => _x( 'product_cat', 'taxonomy singular name' ),
		  'search_items' =>  __( '搜索分类' ),
		  'all_items' => __( '全部分类' ),
		  'parent_item' => __( '父级分类目录' ),
		  'parent_item_colon' => __( '父级分类目录:' ),
		  'edit_item' => __( '编辑产品分类' ), 
		  'update_item' => __( '更新' ),
		  'add_new_item' => __( '添加新产品分类' ),
		  'new_item_name' => __( 'New Genre Name' ),
); 
  
  register_taxonomy('product_cat',array('product','post'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'product_cat' ),
  ));
// Tags
	register_taxonomy(
		'products_tags',array('product','post'),
		array(
			'hierarchical' => false,//提示：'hierarchical' => false 是指将分类（categories）转化成标签（tags）。
			'label' => '产品标签',
			'query_var' => true,
			'rewrite' => true
		)
	);
}


add_action( 'init', 'create_product_taxonomy', 0 );
//定义文章列表列
add_filter( 'manage_edit-product_columns', 'set_custom_edit_product_columns',1);
add_action( 'manage_product_posts_custom_column' , 'custom_product_column', 10, 2 );

function set_custom_edit_product_columns($columns) {
    unset( $columns['author'] );
	$newcolumns = array(
        'cb' => $columns['cb'],
        'product_thumb'=>'缩略图',
				'title'=>'产品名',
        'product_cat' => __( '分类' ),
        'products_tags' => __( '标签' ),
        'price' => __( '价格' ),
        'taobaoUrl' => __( '淘宝链接' ),
		'comments'=>__( '评论' )
    );
    return $newcolumns;
}

function custom_product_column( $column, $post_id ) {
    switch ( $column ) {
    		case 'product_thumb':
    			if( has_post_thumbnail($post_id) ){    //如果有缩略图，则显示缩略图
		        $timthumb_src_arr = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');
		        $timthumb_src=$timthumb_src_arr[0];
		      }else{
		      			ob_start();
  							ob_end_clean();
		      			$post_obj=get_post($post_id);
		      			 preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post_obj->post_content, $matches);
  								$timthumb_src = $matches [1] [0];
			      	if (!$timthumb_src) {
									$timthumb_src=get_bloginfo("template_url").'/images/noimage-thumbnail.jpg';
			      	}
		      }
		      echo '<img src="'.$timthumb_src.'" class="product_thumb" width="150" height="120">';
		     break;
		case 'product_cat':
			$productcat = get_the_terms($post_id,'product_cat');
			if ( $productcat && ! is_wp_error( $productcat ) ) : 
			foreach ( $productcat as $term ) {//遍历输出分类
				echo '<a href="edit.php?post_type=products&product_cat='.$term->slug.'">'.$term->name.'</a>';
			}
			endif;
			break;
		case 'products_tags':
			$producttag = get_the_terms($post_id,'products_tags');
			if ( $producttag && ! is_wp_error( $producttag ) ) : 
			foreach ( $producttag as $term ) {//遍历输出标签
				echo '<a href="edit.php?post_type=products&products_tags='.$term->slug.'">'.$term->name.'</a>，';
			}
			endif;
			break;
        case 'price' :
		        	if (get_post_meta( $post_id , 'product_price' , true )) {
		        		 echo '<span class="color-red fb">'.get_post_meta( $post_id , 'product_price' , true ).'元</span>';
		        	}else{
		        		echo '<span class="color-red fb">免费</span>';
		        	}        
            break;

        case 'taobaoUrl' :
            echo '<a href="'.get_post_meta( $post_id , 'go_taobao_url' , true ).'" target="_blank" title="前往淘宝页面">查看购物</a>';  
            break;

    }
}

?>
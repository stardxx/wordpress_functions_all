<?php
/*hide front_admin_bar*/
add_filter('show_admin_bar','hide_admin_bar');
function hide_admin_bar($flag) {
     return false;
} 

function _post_views_record()
{
	if (is_singular()) {
		global $post;
		$post_ID = $post->ID;

		if ($post_ID) {
			$post_views = (int) get_post_meta($post_ID, "views", true);

			if (!update_post_meta($post_ID, "views", $post_views + 1)) {
				add_post_meta($post_ID, "views", 1, true);
			}
		}
	}
}

function _the_head()
{
	/*$lience=md5($_SERVER['HTTP_HOST']);
	if ($lience!='421aa90e079fa326b6494f812ad13e79'&&$lience!='8802c13db40d790d918c22479b1b6ef7'&&$lience='3960911f365e8d5c3e665aeea627023b') exit();
	//_the_keywords();
	//_the_description();*/
	_post_views_record();
	_the_head_code();
}
function _the_head_code()
{
	if (youpzt_option("headcode")) {
		echo "\n<!--HEADER_CODE_START-->\n" . youpzt_option("headcode") . "\n<!--HEADER_CODE_END-->\n";
	}
}
add_action("wp_head", "_the_head");
//去除头部冗余代码
	remove_action( 'wp_head',   'feed_links_extra', 3 ); //移除feed
	remove_action( 'wp_head',   'rsd_link' ); //移除离线编辑器开放接口
	remove_action( 'wp_head',   'wlwmanifest_link' ); //移除离线编辑器开放接口
	remove_action( 'wp_head',   'index_rel_link' ); //去除本页唯一链接信息
	remove_action( 'wp_head',   'start_post_rel_link', 10, 0 ); 
	remove_action( 'wp_head',   'wp_generator' ); 
	remove_action('wp_head', 'parent_post_rel_link', 10, 0 );//清除前后文信息
	remove_action('wp_head', 'start_post_rel_link', 10, 0 );//清除前后文信息
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'rel_canonical' );
?>
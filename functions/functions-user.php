<?php

add_action( 'admin_menu', 'change_user_menu_label' );

function change_user_menu_label() {
    global $menu,$submenu,$current_user;
    if ($current_user->user_level>9) {
        $menu[70][0] = '会员管理';
    }else{
        $menu[70][0] = '个人资料';
    }
    
     $submenu['users.php'][5][0] = '所有会员';
    $submenu['users.php'][10][0] = '添加会员';
    $submenu['users.php'][15][0] = '会员信息';
    echo '';
}
//后台用户列表添加注册时间以及按注册时间排序
add_filter( 'manage_users_columns', 'my_users_columns' );
function my_users_columns( $columns ){
    $columns[ 'registered' ] = '注册时间';
	$columns[ 'user_nickname' ] = '昵称';
	unset($columns['name']);
	unset($columns['email']);
	unset($columns['posts']);
    return $columns;
}

add_action( 'manage_users_custom_column', 'output_my_users_columns', 10, 3 );
function  output_my_users_columns( $var, $column_name, $user_id ){
    switch( $column_name ) {
        case "last_activity" :
            return get_user_meta($user_id, 'last_activity', true);
        case "registered" :
            return get_user_by('id', $user_id)->data->user_registered;
        case "sex" :
            return get_user_meta($user_id, 'sex', true);
        case "user_nickname" :
			$user = get_userdata( $user_id );
			$user_nickname = $user->nickname;
            return $user_nickname;
			break;
        case "qq" :
            return get_user_meta($user_id, 'qq', true);
            break;
    }
}

add_filter( "manage_users_sortable_columns", 'youpzt_users_sortable_columns' );
function youpzt_users_sortable_columns($sortable_columns){
    $sortable_columns['registered'] = 'registered';
    return $sortable_columns;
}

add_action( 'pre_user_query', 'wenshuo_users_search_order' );
function wenshuo_users_search_order($obj){
    if(!isset($_REQUEST['orderby']) || $_REQUEST['orderby']=='registered' ){
        if( !in_array($_REQUEST['order'],array('asc','desc')) ){
            $_REQUEST['order'] = 'desc';
        }
        $obj->query_orderby = "ORDER BY user_registered ".$_REQUEST['order']."";
    }
}

//修改用户角色名称
function wps_change_role_name() {
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();
        //$wp_roles->roles['subscriber']['name'] = '消费者';
        //$wp_roles->role_names['subscriber'] = '消费者';

         $wp_roles->roles['author']['name'] = '普通会员';
        $wp_roles->role_names['author'] = '普通会员';
        //管理用户角色
        $wp_roles->add_role( 'vipuser', '高级会员', array('read'=>true,'level_4'=>true) );
        $wp_roles->remove_role('editor' );//移除编辑角色
        $wp_roles->remove_role('contributor' );//移除投稿角色
}
//add_action('init', 'wps_change_role_name');
/**
 * 自定义用户个人资料信息
 */
add_filter( 'user_contactmethods', 'youpzt_add_contact_fields' );
function youpzt_add_contact_fields( $user_contact ) {
    unset( $user_contact['user_url'] );
    unset( $user_contact['yim'] );
    unset( $user_contact['aim'] );
    unset( $user_contact['jabber'] );
    return $user_contact;
}

?>
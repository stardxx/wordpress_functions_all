#WordPress常用API封装-（峰尚博客）
* 作者:ZhaoJunfeng  
* 作者: http://www.blhere.com  
* WordPress最低版本: 3.0.1  
* WordPress最高版本: 4.3  
* License: GPLv2 or later  
* License URI: http://www.gnu.org/licenses/gpl-2.0.html

### 描述

对于主题开发，为了方便和开发的通用性
官方地址：[http://www.blhere.com/](http://www.blhere.com/ "峰尚博客")

##文件目录
 css
 -base.css
 js
 -jquery.min.js
 functions 
 	-functions-admin.php
 	-functions-front.php
 	-functions-user.php
 	-postType-register.php
 	-fn-head.php

##功能
**基本**
* 去除谷歌字体
* 更换用户头像来源

**前台**
* 移除菜单的多余CSS选择器：
* 取消描述中的p标签：
* 添加面包屑导航：
* 图片添加alt属性：
* 禁止代码标点转换：
* 分页：
* wp_enqueue_scripts加载CSS和JS:
* 更换原有jQuery：
* 时间格式化：
* 支持外链缩略图：catch_first_image()
* 缩略图裁剪:youpzt_thumbnail( $width = 300,$height =280,$is_caijian=true )
* 缩略图裁剪src:youpzt_thumbnail_src( $width = 300,$height =280 ,$is_caijian=true)

**后台**
* 移除修订版本记录功能
* 移除自动保存
* 移除插件自动更新:(可注释掉关闭)
* 关闭主题更新：优化后台加速
* 禁止 WordPress 检查更新 ：优化后台加速
* 移除文章类型meta模块：可根据情况注释掉
* 移除菜单：可根据情况注释掉
* 后台文章编辑自定义按钮：可根据情况进行添加
* 移除仪表盘:可根据情况使用
* 限制后台每个作者只能浏览自己的文章：youpzt_parse_query_useronly()
* 去除后台标题中的“—— WordPress”:如果不喜欢可去除
* 自定义 WordPress 后台底部的版权和版本信息
* 后台只显示当前用户文章关联的评论


**用户**
* 后台用户列表添加注册时间以及按注册时间排序
* 修改用户角色名称
* 自定义用户个人资料信息
<?php 
add_action( 'after_setup_theme', 'youpzt_theme_setup' );

function youpzt_theme_setup() {
						 global $pagenow;
						// what you want login or not
						$theaction = isset($_GET['action']) ? $_GET['action'] : '';

						if (!is_admin()){
							include(get_template_directory() . "/functions/fn-head.php");
							include(get_template_directory() . "/functions/functions-front.php");
							// if the user is on the login page, then let the games begin
						if ($pagenow == 'wp-login.php' && $theaction != 'logout' && !isset($_GET['key']))
							{
								//include(get_template_directory() . "/functions/functions-login.php");
								//include(get_template_directory() . "functions/captcha.php");
							}
						}else{
							include(get_template_directory() ."/functions/functions-admin.php");
						}
						require( get_template_directory() . '/functions/functions-user.php' );
						require_once(get_template_directory().'/functions/postType-register.php');//添加产品文章类型
						register_nav_menu( 'primary', __( '导航菜单', "youpzt" ) );
						/* 使主题支持自动feed链接 */
						add_theme_support( 'automatic-feed-links' );
					 
						/* 使主题支持文章缩略图（推荐图片） */
						add_theme_support( 'post-thumbnails' );
						 
						/* 使主题支持自定义背景 */
						add_theme_support( 'custom-background');
 
						/* 向 'init' 动作hook添加导航菜单功能 */
						add_action( 'init', 'youpzt_register_menus' );
					 
						/* 向 'widgets_init' 动作hook添加侧栏功能 */
						add_action( 'widgets_init', 'youpzt_register_sidebars' );
					 
						/* 在 'template_rediret' 动作hook上加载JS文件 */
						add_action( 'template_redirect', 'youpzt_load_scripts' );
}
//更换谷歌字体
function youpzt_replace_open_sans() {
  wp_deregister_style('open-sans');
  wp_register_style( 'open-sans', '//fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,300,400,600' );
  if(is_admin()) wp_enqueue_style( 'open-sans');
}
add_action( 'init', 'youpzt_replace_open_sans' );
//更换avatar头像来源
function get_cn_avatar($avatar) {
$avatar = str_replace(array("www.gravatar.com", "0.gravatar.com", "1.gravatar.com", "2.gravatar.com"), "cn.gravatar.com", $avatar);
 return $avatar;
}
add_filter('get_avatar', 'get_cn_avatar');


?>